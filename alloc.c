

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef struct _meta_Data {
  struct _meta_Data * freeNext;
  struct _meta_Data * freePrev;
  char isFree;
  size_t size;
}metaData;



typedef struct _tail{
  size_t size;
  char isFree;
}tail;


metaData *freeList = NULL;
metaData* begHeap = NULL;
metaData* endHeap = NULL;
int begInit =0;
/**
 * Allocate space for array in memory
 *
 * Allocates a block of memory for an array of num elements, each of them size
 * bytes long, and initializes all its bits to zero. The effective result is
 * the allocation of an zero-initialized memory block of (num * size) bytes.
 *
 * @param num
 *    Number of elements to be allocated.
 * @param size
 *    Size of elements.
 *
 * @return
 *    A pointer to the memory block allocated by the function.
 *
 *    The type of this pointer is always void*, which can be cast to the
 *    desired type of data pointer in order to be dereferenceable.
 *
 *    If the function failed to allocate the requested block of memory, a
 *    NULL pointer is returned.
 *
 * @see http://www.cplusplus.com/reference/clibrary/cstdlib/calloc/
 */
void *calloc(size_t num, size_t size) {
  // implement calloc!
  size_t totalBytesNeeded = num*size;
  void* result = malloc(totalBytesNeeded);
  if(!result) return NULL;
  memset(result,0,totalBytesNeeded);

  return result;
}

/**
 * Allocate memory block
 *
 * Allocates a block of size bytes of memory, returning a pointer to the
 * beginning of the block.  The content of the newly allocated block of
 * memory is not initialized, remaining with indeterminate values.
 *
 * @param size
 *    Size of the memory block, in bytes.
 *
 * @return
 *    On success, a pointer to the memory block allocated by the function.
 *
 *    The type of this pointer is always void*, which can be cast to the
 *    desired type of data pointer in order to be dereferenceable.
 *
 *    If the function failed to allocate the requested block of memory,
 *    a null pointer is returned.
 *
 * @see http://www.cplusplus.com/reference/clibrary/cstdlib/malloc/
 */
tail* getTail(metaData* ptr){

  return (tail*)((char*)ptr + ptr->size + sizeof(metaData));
}

void insertFreePtr(metaData* ptr){
  ptr->isFree=1; //change head isFree to 1
  ((tail*)((char*)ptr + ptr->size + sizeof(metaData)))->isFree=1; // change it's tail
  if(freeList == NULL){ //if the list is empty, then add to the list
    freeList=ptr; //assumming ptr already exists
    ptr->freeNext=NULL;
    ptr->freePrev=NULL;
  }
  else{
    freeList->freePrev = ptr;
    ptr->freeNext = freeList;
    ptr->freePrev = NULL;
    freeList = ptr;
  }
}

void removeFreePtr(metaData* ptr){
  /*if(freeList == NULL || ptr==NULL){ //if list is Empty or ptr is NULL
    return -1;
  }
  metaData* curr = freeList;
  metaData* prev = NULL;
  while(curr!=NULL && curr!=ptr){
    prev = curr;
    curr = curr->freeNext;
  }
  if(curr == NULL){
    write(1, "FreePtr not Found in List!\n",27 );
    return -1;
  }
  curr->isFree=0;
  if(prev!=NULL) prev->freeNext = curr->freeNext;
  if(curr->freeNext!=NULL) curr->freeNext->freePrev = prev;
  if(curr->freeNext==NULL && curr->freePrev==NULL) {
    freeList=NULL;
    return 1;
  }
  // write(1,"removeFree\n",11);
  tail* tailPtr = (tail*)((void*)curr + sizeof(metaData) + curr->size);
  tailPtr->isFree =0;
  return 0;*/
  if(freeList == NULL || ptr == NULL)return;
  if(freeList==ptr) freeList =ptr->freeNext;

  if(ptr->freeNext!=NULL)
    ptr->freeNext->freePrev = ptr->freePrev;

  if(ptr->freePrev!=NULL)
    ptr->freePrev->freeNext = ptr->freeNext;

  ptr->isFree=0;
  // write(1,"removeFree\n",11);
  getTail(ptr)->isFree=0;
  return;
}

metaData* findFree(size_t size){
  if(freeList==NULL){
    return NULL;
  }
  metaData* curr = freeList;
  while(curr!=NULL && !(curr->isFree == 1 && curr->size>=size)){
    curr = curr->freeNext;
  }
  if(curr==NULL) return NULL;
  else return curr;

}



void split(metaData* ptr, size_t size){
  //split the current ptr and add the split portion to free
  //Remember to update the tails
  // write(1,"Split!\n", 7);
  tail* oldTail = getTail(ptr);
  tail* newTail = (tail*)((void*)ptr + sizeof(metaData) + size);
  metaData* newHead = (metaData*)((void*)newTail + sizeof(tail));

  size_t newSize = ptr->size - sizeof(tail) - sizeof(metaData) - size;

  newHead->size = newSize;
  oldTail->size = newSize;

  newTail->size = size;
  ptr->size = size;
  removeFreePtr(ptr);
  insertFreePtr(newHead);
}
/*
int isInFreeList(metaData* ptr){
  if(ptr==NULL) return 0;
  metaData* curr = freeList;
  while(curr!=NULL && curr!=ptr){
    curr = curr->freeNext;
  }
  if(curr==NULL) return 0;
  else return 1;
}*/
void *malloc(size_t size) {
  // implement malloc!
  //Need to Allign Size!!
  // printf("Passed Loop!\n");
  int alignment = 8;
  size_t memory = size;
  size_t memorySize = (alignment-memory%alignment) + memory;

  // metaData* curr = findFree(memorySize);
  // write(1,curr,sizeof(curr)+1);
  // write(1,"\n",1);
  metaData* curr = freeList;
  while(curr!=NULL && !(curr->isFree==1 && curr->size>=size)){
    curr = curr->freeNext;
  }
  if(curr==NULL){
    // write(1,"sbrking\n",8);
    curr = sbrk(memorySize + sizeof(metaData) + sizeof(tail));
    if(begInit == 0) {
      begHeap = curr;
      begInit =1;
    }
    endHeap = curr;
    curr->freeNext = NULL;
    curr->freePrev = NULL;
    curr->isFree = 0;
    curr->size = memorySize;
    // write(1,"Moved..\n",8);
    //Initialize the tail
    // write(1,"sbrk\n", 5);
    tail* tailPtr = getTail(curr);
    tailPtr->size = memorySize;
    tailPtr->isFree = 0;
    return (void*)(curr+1);
  }
  else{
    // write(1,"Found Free!\n",12);
    if(curr->size>(memorySize + sizeof(metaData) + sizeof(tail))){
      // split(curr, memorySize);
    }
    removeFreePtr(curr);
    return (void*)(curr+1);
  }
}

/**
 * Deallocate space in memory
 *
 * A block of memory previously allocated using a call to malloc(),
 * calloc() or realloc() is deallocated, making it available again for
 * further allocations.
 *
 * Notice that this function leaves the value of ptr unchanged, hence
 * it still points to the same (now invalid) location, and not to the
 * null pointer.
 *
 * @param ptr
 *    Pointer to a memory block previously allocated with malloc(),
 *    calloc() or realloc() to be deallocated.  If a null pointer is
 *    passed as argument, no action occurs.
 */
void free(void *ptr) {
  // implement free!
  /*
  void* metaPtr = (metaData*)ptr-1;
  metaData* head = (metaData*) metaPtr;
  // write(1,"tailPtr\n", 8);
  tail* tailPtr = (tail*)(head+1);
  int leftFree = 0;
  int rightFree = 0;
  tailPtr = (void*)tailPtr + head->size;
  tail* leftTail = (tail*)((tail*)metaPtr-1);
  metaData* leftHead = (metaData*)((void*)leftTail - leftTail->size - sizeof(metaData));
  metaData* rightHead = (metaData*)(tailPtr+1);
  // write(1,"rightTail\n",10);
  tail* rightTail = (tail*)((void*)rightHead + sizeof(metaData) + rightHead->size);
  */
  //edge cases to check for: check if beginning of heap, check if end of heap

  metaData* head = (metaData*)(ptr - sizeof(metaData));
  // write(1,"freeTail\n", 9);
  tail* tailPtr = getTail(head);


  metaData* rightHead = (metaData*)(tailPtr+1);
  // write(1,"freeHead\n", 9);
  tail* rightTail = NULL;
  if(head!=endHeap)rightTail = getTail(rightHead);
  // write(1,"after\n", 6);
  tail* leftTail = (tail*)((char*)head - sizeof(tail));
  metaData* leftHead = (metaData*)((char*)leftTail - leftTail->size);

  int leftFree=0;
  int rightFree=0;

  if(head == begHeap || head == endHeap){
    if(head == begHeap){ //begHeap only has right, no left
      if(rightHead->isFree) rightFree=1;
    }
    else{  //endHeap only has left, no right
      if(leftTail->isFree) leftFree=1;
    }
  }
  else{
    if(leftTail->isFree) leftFree=1;
    if(rightHead->isFree) rightFree=1;
  }

  if(leftFree==0 && rightFree==1){
    head->size += sizeof(metaData) + sizeof(tail) + rightHead->size;
    rightTail->size = head->size;
    removeFreePtr(rightHead);
    insertFreePtr(head);
  }
  else if(leftFree==1 && rightFree==0){
    leftHead->size += sizeof(metaData) + sizeof(tail) + head->size;
    tailPtr->size = leftHead->size;
  }
  else if(leftFree==1 && rightFree==1){
    leftHead->size += 2*sizeof(metaData) + 2*sizeof(tail) + head->size + rightHead->size;
    rightTail->size = leftHead->size;
    removeFreePtr(rightHead);
  }
  else{
    insertFreePtr(head);
  }


}

/**
 * Reallocate memory block
 *
 * The size of the memory block pointed to by the ptr parameter is changed
 * to the size bytes, expanding or reducing the amount of memory available
 * in the block.
 *
 * The function may move the memory block to a new location, in which case
 * the new location is returned. The content of the memory block is preserved
 * up to the lesser of the new and old sizes, even if the block is moved. If
 * the new size is larger, the value of the newly allocated portion is
 * indeterminate.
 *
 * In case that ptr is NULL, the function behaves exactly as malloc, assigning
 * a new block of size bytes and returning a pointer to the beginning of it.
 *
 * In case that the size is 0, the memory previously allocated in ptr is
 * deallocated as if a call to free was made, and a NULL pointer is returned.
 *
 * @param ptr
 *    Pointer to a memory block previously allocated with malloc(), calloc()
 *    or realloc() to be reallocated.
 *
 *    If this is NULL, a new block is allocated and a pointer to it is
 *    returned by the function.
 *
 * @param size
 *    New size for the memory block, in bytes.
 *
 *    If it is 0 and ptr points to an existing block of memory, the memory
 *    block pointed by ptr is deallocated and a NULL pointer is returned.
 *
 * @return
 *    A pointer to the reallocated memory block, which may be either the
 *    same as the ptr argument or a new location.
 *
 *    The type of this pointer is void*, which can be cast to the desired
 *    type of data pointer in order to be dereferenceable.
 *
 *    If the function failed to allocate the requested block of memory,
 *    a NULL pointer is returned, and the memory block pointed to by
 *    argument ptr is left unchanged.
 *
 * @see http://www.cplusplus.com/reference/clibrary/cstdlib/realloc/
 */
void *realloc(void *ptr, size_t size) {
  // implement realloc!

  int alignment = 8;
  size_t memory = size;
  size_t memorySize = (alignment-memory%alignment) + memory;


  if(ptr==NULL){
    ptr = malloc(memorySize);
    return ptr;
  }
  if(memorySize==0 && ptr!=NULL){
    free(ptr);
    return NULL;
  }
  metaData* metaPtr = (metaData*)ptr-1;
  if(metaPtr->size == memorySize){
    return ptr;
  }
  else if(metaPtr->size < memorySize){
    char* voidPtr = malloc(memorySize);
    memmove(voidPtr,ptr,metaPtr->size);
    free(ptr);
    return voidPtr;
  }
  else{
    // split(metaPtr,memorySize);
    return (void*)(metaPtr+1);
  }

  return NULL;
}
