
 #include <stdio.h>
 #include <unistd.h>
 #include <string.h>
 #include <ctype.h>
 #include <stdlib.h>
 #include <signal.h>
 #include <sys/types.h>
#include <sys/wait.h>
#include "format.h"
#include "log.h"
#include "shell.h"

FILE* hp=NULL;
FILE* fp=NULL;
char historyFileName[50];
char runFileName[50];
Log* myLog;
char** freedude=NULL;

void cleanup(int signal){
  // int status;
  while(waitpid((pid_t) -1, 0,WNOHANG)>0){}
}
void executeCommand(const char* command){
  int firstIndex=0;
  int isBuiltIn=0;
  char firstFew[40];
  char entireCommand[400];
  for(int i=0; i<40; i++){
    firstFew[i]=0;
  }
  memset(entireCommand,0,400);
  int index=0;
  if(isspace(command[0])){
    while(isspace(command[index])){
      firstIndex++;
      index++;
    }
  }
  for(int i=firstIndex; i<(int)strlen(command); i++){
    if(!isspace(command[i])){
      firstFew[i-firstIndex]=command[i];
      firstFew[i+1]=0;
      entireCommand[i-firstIndex]=command[i];
      entireCommand[i+1]=0;
    }
    else{
      entireCommand[i-firstIndex]=command[i];
    }
  }

  if(strcmp(firstFew,"cd")==0 || firstFew[0]=='!' || firstFew[0]=='#'){
    isBuiltIn=1;
  }
  // printf("Is Built-In?? %d\n", isBuiltIn );

  // printf("%s\n", entireCommand );

  char commandArr[20][90];
  // for(int i=0; i<20; i++){
  //   commandArr[i]=NULL;
  // }
  memset(commandArr,0,1800);
	char* token = strtok(entireCommand," ");
	int x=0;
	while(token!=NULL){
		// commandArr[x]=(char*)malloc(strlen(token)+1);
		strcpy(commandArr[x],token);
		commandArr[x][strlen(token)]=0;
		// printf("Adding...%s to %d\n", token, x );
		token = strtok(NULL," ");
		x++;
	}


if(isBuiltIn){ //if the command if Built-In
  if(strcmp(commandArr[0],"cd")==0){ //cd command
    if(commandArr[1]==NULL){
    print_no_directory(commandArr[0]);
    }
    else if(commandArr[1][0]=='/'){
      int status = chdir(commandArr[1]);
      if(status==-1){
        print_no_directory(commandArr[1]);
      }
    }
    else{
      int status = chdir(commandArr[1]);
      if(status==-1){
        print_no_directory(commandArr[1]);
      }
    }
    Log_add_command(myLog, command);
  }
  else if(strcmp(commandArr[0],"!history")==0){
    //prints out the history file
    for(int i=0; i<(int)Log_size(myLog); i++){
      print_history_line(i,Log_get_command(myLog,i));
    }
  }

  else if(commandArr[0][0]=='!'){
      char temp[100];
      memset(temp,0,100);
      strcpy(temp,(commandArr[0]+1));
      printf("%s\n", temp );
      int found=0;
      for(int i=(int)Log_size(myLog)-1; i>=0; i--){
        if(strstr(Log_get_command(myLog,i),temp)!=NULL){
          executeCommand(Log_get_command(myLog,i));
          // char myCmd[100];
          //  strcpy(myCmd,Log_get_command(myLog,i));
          // Log_add_command(myLog,myCmd);
          found=1;
          break;
        }
      }
      if(!found){
        print_no_history_match();
      }
  }
  else if(commandArr[0][0]=='#'){
      int runLine =0;
      // char temp[5];
      if(commandArr[0][1]==0 || !isdigit(commandArr[0][1])){
          //do nothing
      }
      else {

        // printf("%s\n", commandArr[0] );
        runLine=atoi((commandArr[0]+1));
        if(runLine<(int)Log_size(myLog)){
          executeCommand(Log_get_command(myLog,runLine));
          // char myCmd[100];
          //  strcpy(myCmd,Log_get_command(myLog,runLine));
          // Log_add_command(myLog,myCmd);
        }
        else print_invalid_index();
      }
      // printf("Runline: %d\n", runLine );

  }
  // for(int i=0; i<(int)sizeof(commandArr); i++){
  //   free(commandArr[i]);
  //   // free (commandArr);
  // }
}
else{ //if the command isn't Built-In
// pid_t parent = getpid();
if(strstr(command,"&")==NULL){
    pid_t pid = fork();

    if (pid == -1)
    {
        // error, failed to fork()
        print_fork_failed();
    }
    else if (pid > 0)
    {
        int status;
       int didFail=  waitpid(pid, &status, 0);
       if(!didFail){
         print_wait_failed();
       }
        // printf("%s\n", );
        Log_add_command(myLog, command);
        print_command_executed(pid);
    }
    else
    {
        // we are the child
        execl("/bin/sh", "/bin/sh", "-c", command, 0);
        print_exec_failed(command);   // exec never returns
    }
}
else{

  signal(SIGCHLD, cleanup);
  pid_t pid = fork();
  if (pid == -1)
  {
      // error, failed to fork()
      print_fork_failed();
  }
  else if (pid > 0)
  {
      //do nothing in parent
      Log_add_command(myLog, command);
      print_command_executed(pid);
  }
  else
  {
      // we are the child
      // execve(...);
      setpgid(0, 0);
      execl("/bin/sh", "/bin/sh", "-c", command, 0);
      print_exec_failed(command);   // exec never returns
  }

}
}

}


int shell(int argc, char *argv[]) {
  // TODO: This is the entry point for your shell.
  signal(SIGCHLD, cleanup);
// print_usage();
print_shell_owner("ryanlai2");
int opt =0;
char filenameH[20];
char filenameF[20];
int fFlag=0;
int hFlag=0;
for(int i=0; i<20; i++){
  filenameF[i]=0;
  filenameH[i]=0;
}


while ((opt = getopt (argc, argv, "f:h:")) != -1){
    switch(opt) {
      case 'f':

      // printf("User flagged f\n");
      strcpy(runFileName,optarg);
      fFlag=1;
      break;

      case 'h':
      // printf("User flagged h\n");
      strcpy(filenameH,optarg);
      hFlag=1;
      break;
      default:
      print_usage();
      break;
    }

}

// printf("filename -h:  %s\n", filenameH);
// printf("filename -f: %s\n", filenameF );
// int isDone =0;
char* line=NULL;
size_t len=0;
ssize_t read=0;
signal(SIGINT,SIG_IGN);
if(hFlag){ //check if there was an h flag
  if(filenameH[0]!=0){
    strcpy(historyFileName,filenameH);
    myLog = Log_create_from_file(historyFileName);

    // printf("created log!!\n" );
  }
}
else{
  // hp=fopen("history.txt","w+");
   myLog = Log_create();
  // printf("New filename: %s\n",historyFileName );
}
while(read!=EOF){
signal(SIGCHLD, cleanup);


  if(fFlag){
    Log* fileLog = Log_create_from_file(runFileName);
    if(Log_size(fileLog)==0){
      print_script_file_error();
    }
    for(int i=0; i<(int)Log_size(fileLog);i++){
      executeCommand(Log_get_command(fileLog,i));
    }
    // printf("HFLAG: %d", hFlag );
    // printf("%s\n", historyFileName );
    if(hFlag){
      // printf("saving history\n" );
      Log_save(myLog,historyFileName);
    }
    return 0;
  }
  char cwd[1024];
  getcwd(cwd, sizeof(cwd));
  // printf("(pid=%d)as$",getpid(), cwd );
  print_prompt(cwd,getpid());
  read = getline(&line, &len, stdin);
  line[strlen(line)-1]=0;

 if(read!=EOF) executeCommand(line);
}
if(hFlag){
  Log_save(myLog,historyFileName);
}
 free(line);
 Log_destroy(myLog);

  return 0;
}
